import config
import pickle, string
from matrix_client.client import MatrixClient, MatrixHttpApi, User

config = config.Config()
matrix = MatrixHttpApi(config.homeserver, config.token)

class ModuleFunctions():
    
    def __init__(self):
        self.config = config
    
    ###PUBLIC FUNCTIONS###
    
    
    
    #returns a users display name
    def get_display_name(self, user_id):
        user = User(matrix, user_id)
        return user.get_display_name()




    #Stop that damn crash
    def filter_nonprintable(self, text):
        return ''.join(filter(lambda x: x in string.printable, text))

    #For relaying info from the chat to the console for debug
    def relay_to_console(self, room, event):
        if event['type'] == "m.room.message":
            if event['content']['msgtype'] == "m.text":
                print(self.filter_nonprintable("{0}: {1}".format(event['sender'], event['content']['body'])))
            else:
                print(event['content']['msgtype'])
        else:
            print(event['type'])



    ###PING###

    def ping(self, room, event):
        if event['type'] == "m.room.message":
            if event['content']['msgtype'] == "m.text":
                if event['content']['body'] == "!ping":
                    room.send_text("PONG!")

    ###NOTIFY###

    def notify(self, room, event):
        if event['type'] == "m.room.message":
            if event['content']['msgtype'] == "m.text":
                if event['content']['body'] == "!subscribe":
                    print(room.room_id)
                    
                    #If room exists
                    if room.room_id in self.config.notify_list:
                        if event['sender'] in self.config.notify_list[room.room_id]:
                            #remove them
                            self.config.notify_list[room.room_id].remove(event['sender'])
                            print(self.config.notify_list[room.room_id])
                            room.send_text(event['sender'] + " removed from notify list!")
                            
                        else:
                            
                            self.config.notify_list[room.room_id].append(event['sender'])
                            print(self.config.notify_list[room.room_id])
                            #Inform user they've been added
                            room.send_text(event['sender'] + " added to notify list!")
                            
                    #Room doesn't exist, so add it then add the user
                    else:
                        self.config.notify_list[room.room_id] = []
                        self.config.notify_list[room.room_id].append(event['sender'])
                    
                        #Inform user they've been added
                        room.send_text(event['sender'] + " added to notify list!")
                    
                    #write list to file
                    with open("notify.pickle", "wb") as fp:
                        pickle.dump(self.config.notify_list, fp)
                    
                if event['content']['body'] == "!notify":
                    #Needs check for user perms here
                    #Check list isn't empty, empty messages are crap
                    try:
                        if self.config.notify_list[room.room_id] != []:

                            notify_string = ""
                            
                            #Just in case empty
                            try:
                                for user in self.config.notify_list[room.room_id]:
                                    username = self.get_display_name(user)
                                    notify_string += username + ", "
                                
                                room.send_text(notify_string)
                            except:
                                pass
                    except:
                        pass
                    
        elif event['type'] == "m.room.member":
            if event['content']['membership'] == "leave":
                #User has left
                try:
                    self.config.notify_list[room.room_id].remove(event['sender'])
                except:
                    pass			
        



    ###EMOTE###

    def emote(self, room, event):
        if event['type'] == "m.room.message":
            if event['content']['msgtype'] == "m.text":
                emote_string = ""
                for emote in self.config.emote_list[room.room_id]:
                    if emote in event['content']['body']:
                        split = self.config.emote_list[room.room_id][emote].split(":")

                        if split[0] == "mxc":
                            #Send the image, it's an MXC
                            room.send_image(self.config.emote_list[room.room_id][emote], emote)
                                
                        else:
                            #Just send the URL
                            emote_string += " " + self.config.emote_list[room.room_id][emote]
                        
                
                #Check the message contains something (otherwise shit spam on every message)
                if emote_string != "":
                    #Send whole thing in 1 message
                    room.send_text(emote_string)		
                    
                    
                    
    ###LEAVE JOIN###

    def leave_join(self, room, event):
        if event['type'] == "m.room.member":
            if event['membership'] == "leave":
                print("Leave")
                room.send_text(self.config.leave_message(get_display_name(event['sender'])))
                
            if event['membership'] == "join":
                print("Join")
                room.send_text(self.config.join_message(get_display_name(event['sender'])))


    ###COMMMANDS###

    def commands(self, room, event):
        if event['type'] == "m.room.message":
            if event['content']['msgtype'] == "m.text":
                
                message = event['content']['body']
                split = message.split( )
                command = split[0]
                
                if command in self.config.commands[room.room_id]:
                    print(self.config.commands[room.room_id][command])
                    room.send_text(self.config.commands[room.room_id][command])


    ##PICKUPS###

    def call_pickup(self, name, players):
        
        player_string = name + " PICKUP HAS STARTED "
        
        for user in players:
            #Clean the vs thing a little
            if user != players[-1]:
                player_string += self.get_display_name(user) + " VS "
            else:
                player_string += self.get_display_name(user)
        
        return player_string

    def pickup_who(self, room):
        player_string = "PICKUPS: "
                    
        for pickup in self.config.pickups[room.room_id]:
            print(pickup)
            player_string += "*" + pickup + "*: "

            if self.config.pickups[room.room_id][pickup][1] != []:
                for user in self.config.pickups[room.room_id][pickup][1]:
                    print(user)
                    player_string += get_display_name(user) + ", "
            else:
                #It's empty so display nothing
                pass
                
            #Add the number of users needed
            player_string += " [ " + str(len(self.config.pickups[room.room_id][pickup][1])) + "/" + str(self.config.pickups[room.room_id][pickup][0]) + " ] "
            
        return player_string

    def remove_players_on_full(self, room, event):

        split = event['content']['body'].split()
        pickup = self.config.pickups[room.room_id][split[1]]

        for user in pickup[1]:
            for pickup in self.config.pickups[room.room_id]:
                if user in self.config.pickups[room.room_id][pickup][1]:
                    self.config.pickups[room.room_id][pickup][1].remove(user)
        
        
    def pickups(self, room, event):
        if event['type'] == "m.room.message":
            if event['content']['msgtype'] == "m.text":
                split = event['content']['body'].split()
                
                if split[0] == "!add":
                    #Check they're adding to something
                    print(len(split))
                    if len(split) > 1:
                        try:
                            pickup = self.config.pickups[room.room_id][split[1]]
                            print("Managed to pickup")
                        except:
                            print("Failed to pickup")
                            pass
                        if event['sender'] in pickup[1]:
                            pass
                        else:
                            
                            
                            
                            try:
                                pickup[1].append(event['sender'])
                                room.send_text("Added " + self.get_display_name(event['sender']) + " to pickup " + split[1])
                                room.send_text(self.pickup_who(room))
                            except:
                                print("Failed to add")
                                pass
                                
                        if len(pickup[1]) == pickup[0]:
                            #Pickup is full
                            print(self.call_pickup(split[1], pickup[1]))
                            room.send_text(self.call_pickup(split[1], pickup[1]))
                            
                            self.remove_players_on_full(room, event)
                            
                            #Hack because it doesn't remove the last player in the list (the current sender)
                            for pickup in self.config.pickups[room.room_id]:
                                if event['sender'] in self.config.pickups[room.room_id][pickup][1]:
                                    self.config.pickups[room.room_id][pickup][1].remove(event['sender'])
                            
                            room.send_text(self.pickup_who(room))
                    
                elif split[0] == "!remove":
                    #Looks absolutely horrible, but works
                    for pickup in self.config.pickups[room.room_id]:
                        if event['sender'] in self.config.pickups[room.room_id][pickup][1]:
                            self.config.pickups[room.room_id][pickup][1].remove(event['sender'])
                        
                    room.send_text("Removed " + self.get_display_name(event['sender']) + " from all pickups" )
                            
                elif split[0] == "!who":
                    room.send_text(self.pickup_who(room))
                        
                    
                
            
            
            
