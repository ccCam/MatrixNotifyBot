class Config():
    def __init__(self):
        #User access token goes here
        self.token = ""

        #User ID goes here. @username:homeserver.tld
        self.user_id = ""

        #Home server. http/s://homeserver.tld
        self.homeserver = ""


        #["notify", "ping", "emote", "leave_join", "commands"]
        #List of rooms and modules you'd like for each. Please use internal room ID
        self.room_modules = {
            "room_id1": ["module1", "module2"], 
            "room_id2": ["module2", "module3"]
        }

        #Dictionary of rooms, to dictionary of emojis and their links
        #Please use internal room ID
        self.emote_list = {
            "room_id1": {
                "emote1": "emote_url"
                "emote2": "emote2_url"
            }
            "room_id2": {
                "emote1": "emote_url"
            }
        }

        self.commands = {
            "room_id1": {
                "!command": "Hello there friend"
                "!command2": "Lol, get rekt"
            }
            "room_id2": {
                "!command1": "Some text goes here"
            }
        }

        self.pickups = {
            "room_id1": {
                #NAME: [players_needed, [LEAVE BLANK]]
                "duel": [2, []],
                "tdm": [8, []]
            }
        }
        
        #Do not edit
        self.notify_list = {}
        self.current_path = ""
        
        self.load_notify



    #Put your message here
    def leave_message(self, name):
        return "Goodbye " + name + " :("

    def join_message(self, name):
        return "Welcome " + name + " :)"
        
    
            #Currently a global notify list. Needs to be somehow made per room
    def load_notify(self):
        #pickle the list from the file
        try:
            if os.path.getsize("notify.pickle") > 0:
                with open("notify.pickle", "rb") as fp:
                    self.notify_list = pickle.load(fp)
                    print(self.notify_list)
        except:
            pass


