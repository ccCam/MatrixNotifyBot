#!/bin/python3
#Matrix Bot

from matrix_client.client import MatrixClient, MatrixHttpApi, User
import config, module_functions
import pickle, os

class Bot():
    


    def load_modules(self):
        for room_name, modules in self.config.room_modules.items():
            room = self.client.join_room(room_name)
            room.add_listener(self.module_functions.relay_to_console)
            if "ping" in modules:
                room.add_listener(self.module_functions.ping)
            if "notify" in modules:
                self.config.load_notify()
                room.add_listener(self.module_functions.notify)
            if "emote" in modules:
                room.add_listener(self.module_functions.emote)
            if "leave_join" in modules:
                room.add_listener(self.module_functions.leave_join)
            if "commands" in modules:
                room.add_listener(self.module_functions.commands)
            if "pickup" in modules:
                room.add_listener(self.module_functions.pickups)

    
    def __init__(self):
        self.config = config.Config()
        print("Login")
        self.client = MatrixClient(self.config.homeserver, token=self.config.token, user_id=self.config.user_id)
        self.matrix = MatrixHttpApi(self.config.homeserver, self.config.token)
        self.user = User(self.matrix, self.config.user_id)
        
        self.module_functions = module_functions.ModuleFunctions()
        
        print("Sync")
        response = self.matrix.sync()

        print("Listening")
        self.load_modules()

        self.client.listen_forever()
        #Nothing can happen after here






if __name__ == '__main__':
    bot = Bot()
