# A Python Matrix bot aiming to add a little colour to Matrix :D

In my mad attempt to bring people from Dicord to Matrix I realised a few things. 
Many things people take for granted in Discord are missing in Matrix, such as 
custom emotes and roles (mostly for notifications). So I decided to do something
about it and make a bot!

Pull requests are much appreciated, features may be denied however (feel free to fork
at any point you like, or if possible add the ability to easily add extra features
without major code changes every time)

Current abilities:  
- Notify: Subscribe to a list of users to be notified when the `!notify` command is used  
- Ping: Simple `!ping` -> `PONG!` command, mostly for testing.  
- Emotes: Custom emotes. 
- Commands: Any custom commands you like  
- Join/Leave message: Whatever you'd like to greet the user. 

Please see the issues regarding certain things as this is still very much a WIP.

#Install Guide  
WIP

#Contribution Guide  
WIP

#Contributors  
[ccCam](https://gitlab.com/ccCam)  